# User configs (change these if you like):
# Toggles the AI
from student_impl import set_gape_size

AI_ENABLED = True
# Speed the game up (default factor = 1)
SPEED_UP_FACTOR = 10
# Toggle sound
ENABLE_SOUND = False
# Verbose console output
VERBOSE = False


# Do not change these configs
FPS = 10
SCREENWIDTH = 288
SCREENHEIGHT = 512
PIPEGAPSIZE = 100  # gap between upper and lower part of pipe

DIFFICULTY = set_gape_size()
if DIFFICULTY == "HARD":
    PIPEGAPSIZE = 100
elif DIFFICULTY == "MEDIUM":
    PIPEGAPSIZE = 125
elif DIFFICULTY == "EASY":
    PIPEGAPSIZE = 150

BASEY = SCREENHEIGHT * 0.79


# Other changes
if AI_ENABLED:
    FPS *= SPEED_UP_FACTOR  # for speeding up the lib
