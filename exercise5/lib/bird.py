from lib.flappy import FlappyGame

def recognition_from_game(game: FlappyGame) -> dict:
    return {"game_object": game,
            "score": game.score,
            "lowerPipes": game.lowerPipes,
            "upperPipes": game.upperPipes,
            "player_x": game.playerx,
            "player_y": game.playery,
            "player_rot": game.player_rot,
            "player_vel_x": game.player_vel_x,
            "player_vel_y": game.player_vel_y,
            "SCREEN": game.SCREEN}
