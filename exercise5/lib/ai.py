import numpy as np
from collections import deque

import pygame
from keras.engine.saving import load_model
from lib.environment import FlappyEnv
from student_impl import get_agent_observation_space, create_agent_model, get_agent_parameters, callback_on_step, draw_game_callback


class DQNAgent:
    def __init__(self, environment):
        # Initialize constant
        self.environment = environment
        self.obs_size = get_agent_observation_space()

        self.action_size = environment.action_space.n
        self.consecutive_episodes = 100

        # Hyperparameters of the training
        hyperparams = get_agent_parameters()
        self.learning_rate = hyperparams['learning_rate']
        self.gamma = hyperparams['gamma']
        self.replay_memory = hyperparams['replay_memory']
        self.replay_size = hyperparams['replay_size']
        self.epsilon = hyperparams['epsilon']
        self.epsilon_decay = hyperparams['epsilon_decay']
        self.epsilon_min = hyperparams['epsilon_min']
        self.episode_b4_replay = hyperparams['episode_b4_replay']

        # Define variable
        self.storage = deque(maxlen=self.replay_memory)
        self.sum_reward = 0
        self.sum_reward_dirty_value, self.rewards_lst, self.avg_reward, self.avg_loss = 0.0, [], 0.0, 0.0
        self.keras_history_lst = []

        # Is set by self.build_model() or load_user_model()
        self.model = None

    def build_model(self):
        self.model = create_agent_model()

    def store(self, state, action, reward, next_state, done):
        # Save history to storage for replay
        cnt_state = np.reshape(state, [1, self.obs_size])
        new_state = np.reshape(next_state, [1, self.obs_size])
        self.storage.append((cnt_state, np.array([action]), np.array([reward]), new_state, np.array([done])))

    def action(self, state, reward, done, episode, training=True):
        # Update cumulative reward
        self.sum_reward_dirty_value += reward
        self.rewards_lst.append(self.sum_reward_dirty_value)
        self.avg_reward = np.mean(self.rewards_lst)
        self.avg_loss = np.mean(self.keras_history_lst)

        # Episode ends
        if done:
            self.sum_reward = self.sum_reward_dirty_value
            self.sum_reward_dirty_value = 0.0
            self.rewards_lst = []
            self.keras_history_lst = []
            self.epsilon = max(self.epsilon_decay * self.epsilon, self.epsilon_min)
            return -1

        # Episode ends
        cnt_state = np.reshape(state, [1, self.obs_size])

        # Train agent
        if training:
            if episode >= self.episode_b4_replay:
                self.replay()
                if np.random.random() < self.epsilon:
                    action = self.environment.action_space.sample()
                else:
                    act_values = self.model.predict(cnt_state)
                    action = np.argmax(act_values[0])
            else:
                action = self.environment.action_space.sample()

        # Run trained agent
        else:
            act_values = self.model.predict(cnt_state)
            action = np.argmax(act_values[0])
            print("act_values=", act_values)
        return action

    def replay(self):
        minibatch_idx = np.random.permutation(len(self.storage))[: self.replay_size]

        states = np.concatenate([self.storage[i][0] for i in minibatch_idx], axis=0)
        actions = np.concatenate([self.storage[i][1] for i in minibatch_idx], axis=0)
        rewards = np.concatenate([self.storage[i][2] for i in minibatch_idx], axis=0)
        next_states = np.concatenate([self.storage[i][3] for i in minibatch_idx], axis=0)
        dones = np.concatenate([self.storage[i][4] for i in minibatch_idx], axis=0)

        X_batch = np.copy(states)
        Y_batch = np.zeros((self.replay_size, self.action_size), dtype=np.float64)

        qValues_batch = self.model.predict(states)
        qValuesNewState_batch = self.model.predict(next_states)

        targetValue_batch = np.copy(rewards)
        targetValue_batch += (1 - dones) * self.gamma * np.amax(qValuesNewState_batch, axis=1)

        for idx in range(self.replay_size):
            targetValue = targetValue_batch[idx]
            Y_sample = qValues_batch[idx]
            Y_sample[actions[idx]] = targetValue
            Y_batch[idx] = Y_sample

            if dones[idx]:
                X_batch = np.append(X_batch, np.reshape(np.copy(next_states[idx]), (1, self.obs_size)), axis=0)
                Y_batch = np.append(Y_batch, np.array([[rewards[idx]] * self.action_size]), axis=0)

        keras_history = self.model.fit(X_batch, Y_batch, batch_size=len(X_batch), epochs=1, verbose=0)
        self.keras_history_lst.append(keras_history.history['loss'][0])
        pass

    def save_model(self, filename):
        self.model.save(filename)

    def load_user_model(self, filename):
        self.model = load_model(filename)
        print(self.model.summary())


class FlappyAIManager:
    def __init__(self):
        self.agent = None
        self.memory = None
        self.env = None
        self.nb_steps = None
        self.initialized = False
        self._fpsclock = pygame.time.Clock()

    def train_ai(self, game):
        if self.initialized:
            return

        env = FlappyEnv(game)
        agent = DQNAgent(env)
        agent.build_model()
        episode = 0

        training = True
        while training:
            episode += 1
            state, reward, done = env.reset(), 0.0, False
            action = agent.action(state, reward, done, episode)
            while not done:
                env.render()
                next_state, reward, done, info = env.step(action)
                agent.store(state, action, reward, next_state, done)
                state = next_state
                action = agent.action(state, reward, done, episode)

                # Pass agent back to student
                info.update({"agent": agent})
                # Trigger callback on step
                callback_on_step(info, info['crashed'], episode, info['steps'])
                # Trigger callback to draw game
                draw_game_callback(info['SCREEN'], info)

    def run_trained_ai(self, game,
                       model_name: str,
                       maxscore=1000,
                       fps50=False,
                       show_student_draw=False,
                       EPISODES=5):
        env = FlappyEnv(game)
        agent = DQNAgent(env)
        agent.load_user_model(model_name)
        score_lst = []
        for episode in range(EPISODES):
            state, reward, done = env.reset(), 0.0, False
            action = agent.action(state, reward, done, episode, training=False)
            info = None
            while not done:
                env.render()
                next_state, reward, done, info = env.step(action)
                state = next_state
                action = agent.action(state, reward, done, episode, training=False)

                # Pass agent back to student
                info.update({"agent": agent})
                # Trigger callback to draw game
                if show_student_draw:
                    draw_game_callback(info['SCREEN'], info)
                pygame.display.update()
                if fps50:
                    self._fpsclock.tick(50)
                if info['score'] == maxscore:
                    print("++ Trained model succeded at", maxscore, "score!!")
                    env.close()
                    return
            print("Finished episode", episode, "with score", info['score'])
            score_lst.append(info['score'])

        print("Average score over", EPISODES, "episodes:", np.mean(score_lst))
        env.close()
