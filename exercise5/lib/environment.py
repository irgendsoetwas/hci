from rl.core import Env

from lib.flappy import FlappyGame
from lib.bird import recognition_from_game
from lib.config import VERBOSE


class FlappyEnv(Env):
    def __init__(self, game: FlappyGame):
        """Ctor.
        Define action and observation space."""
        super(Env, self).__init__()

        self.game = game
        self.steps = 0
        self.MAX_SCORE = 0
        self.prev_score = 0

        self.action_space = self.observation_space = self.reward_range = None
        self.define_action_space()
        self.define_observation_space()
        self.define_reward_range()

        if VERBOSE:
            print("\t++ Initialized FlappyEnv successfully ...")

    def define_observation_space(self):
        from student_impl import get_agent_observation_space
        self.observation_space = get_agent_observation_space()

    def define_action_space(self):
        from student_impl import get_agent_action_space
        self.action_space = get_agent_action_space()

    def define_reward_range(self):
        from student_impl import get_agent_reward_space
        self.reward_range = get_agent_reward_space()

    def step(self, action) -> [object, float, bool, dict]:
        """Execute one time step within the environment"""
        from student_impl import get_agent_observation, get_agent_reward
        assert 0 <= action < self.action_space.n
        if VERBOSE:
            print("\n\t++ Stepping FlappyEnv (step = %d) ..." % self.steps, end='')
            print(" action: " + str(action), end='')
        self.steps += 1

        crashtest = self.game.step_game(True if action == 1 else False)

        data = recognition_from_game(self.game)
        data.update(crashtest)
        data.update({"travelled_distance": self.game.travelled_distance})
        data.update({"steps": self.steps})
        data.update({"player_w": 34})
        data.update({"player_h": 24})
        data.update({"pipe_w": 52})
        data.update({"pipe_h": 320})
        data.update({"player_center_x": self.game.playerx + data['player_w'] / 2})
        data.update({"player_center_y": self.game.playery + data['player_h'] / 2})

        observation = get_agent_observation(data)

        reward = get_agent_reward(data)

        data.update({"reward": reward})
        data.update({"observation": observation})

        done = crashtest['crashed']

        if self.game.score > self.MAX_SCORE:
            self.MAX_SCORE = self.game.score
            pass
            #print("!! New highscore:", self.MAX_SCORE, ", distance", self.game.travelled_distance)

        info = data
        return observation, reward, done, info

    def reset(self) -> object:
        """Reset the state of the environment to an initial state"""
        if VERBOSE:
            print("\t++ Resetting FlappyEnv ...")
        self.game = FlappyGame()
        self.game.init_game()
        self.steps = 0

        observation = self.game.observation
        return observation

    def render(self, mode='human', close=False):
        """Render the environment to the screen
            mode (str): The mode to render with.
            close (bool): Close all open renderings.
        """
        if VERBOSE:
            print("\t++ Rendering FlappyEnv ...")
        self.game.render_game()
        return

    def close(self):
        if VERBOSE:
            print("\t++ FLAPPYENV CLOSE ++")
        # Garbage collection will clean up for us
        self.game = None
        pass
