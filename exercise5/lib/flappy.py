import time
from sys import exit
from lib.game import getRandomPipe, showScore, checkCrash, showWelcomeAnimation
from lib.config import AI_ENABLED, SCREENWIDTH, SCREENHEIGHT, ENABLE_SOUND, BASEY

import pygame

from student_impl import get_agent_observation_space

GAME = AI = None

SCREEN = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT))
FPSCLOCK = pygame.time.Clock()
EPOCH = 0


class FlappyGame:
    def __init__(self):
        from itertools import cycle
        from lib.assets import IMAGES
        global SCREEN, FPSCLOCK

        self.observation = [0 for i in range(get_agent_observation_space())]
        self.playerx = 0
        self.playery = SCREENHEIGHT / 2 - 50
        self.upperPipes = None
        self.lowerPipes = None
        self.SCREEN = SCREEN

        # Some score that gets incremented for each rendered frame that the player did not crash
        self.travelled_distance = 0
        self.loopIter = 0
        self.playerIndex = 0
        self.score = 0

        # player velocity, max velocity, downward accleration, accleration on flap
        self.player_vel_x = -4
        self.player_vel_y = -9  # player's velocity along Y, default same as player_flapped
        self.player_max_vel_y = 10  # max vel along Y, max descend speed
        self.player_min_vel_y = -8  # min vel along Y, max ascend speed
        self.player_acc_y = 1  # players downward acceleration
        self.player_rot = 45  # player's rotation
        self.player_vel_rot = 3  # angular speed
        self.player_rot_thr = 20  # rotation threshold
        self.player_flap_acc = -9  # players speed on flapping
        self.player_flapped = False  # True when player flaps
        self.playerIndexGen = cycle([0, 1, 2, 1])
        self.basex = 0
        self.baseShift = IMAGES['base'].get_width() - IMAGES['background'].get_width()
        self.EPOCH = 0
        self.player_vel_y = 0

        self.initialized = False

        SCREEN = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT))
        FPSCLOCK = pygame.time.Clock()

    def init_game(self):
        from lib.assets import IMAGES
        global SCREEN, FPSCLOCK
        pygame.init()
        pygame.display.set_caption('HCI Bird')
        pygame.display.init()

        if not AI_ENABLED:
            movementInfo = showWelcomeAnimation()
            try:
                self.playerIndexGen = movementInfo['playerIndexGen']
                self.playerx, self.playery = int(SCREENWIDTH * 0.2), movementInfo['playery']
                self.basex = movementInfo['basex']
                self.baseShift = IMAGES['base'].get_width() - IMAGES['background'].get_width()
            except UnboundLocalError as le:
                # Dear Kids,
                # in practice, please never catch an error and do nothing with it.
                # Or a bugged demon will come and haunt you in your sleep.
                # Here, we do it, as these variables are not initialized in the first step of the game.
                # Hence we leave them at their defaults that were initialized.
                # Also this code is a mess.
                # (Not my fault!)
                # xoxo,
                #      < the bugged demon >
                pass


        # get 2 new pipes to add to upperPipes lowerPipes list
        newPipe1 = getRandomPipe()
        newPipe2 = getRandomPipe()

        # list of upper pipes
        self.upperPipes = [
            {'x': SCREENWIDTH + 200, 'y': newPipe1[0]['y']},
            {'x': SCREENWIDTH + 200 + (SCREENWIDTH / 2), 'y': newPipe2[0]['y']},
        ]

        # list of lowerpipe
        self.lowerPipes = [
            {'x': SCREENWIDTH + 200, 'y': newPipe1[1]['y']},
            {'x': SCREENWIDTH + 200 + (SCREENWIDTH / 2), 'y': newPipe2[1]['y']},
        ]

        self.initialized = True

        # Main Game Loop
        # Restarts the game via main_game() when the bird crashes
        while not AI_ENABLED:
            crashInfo = self.step_game()
            if crashInfo:
                time.sleep(0.2)
                print("Game over after distance: " + str(self.travelled_distance))
                return crashInfo

    def step_game(self, jump: bool = False) -> dict:
        from lib.assets import SOUNDS, IMAGES

        if AI_ENABLED:
            pygame.event.pump()
            if jump:
                self.player_vel_y = self.player_flap_acc
                self.player_flapped = True
                if ENABLE_SOUND and len(SOUNDS) > 0:
                    SOUNDS['wing'].play()
        else:  # Human Player
            for event in pygame.event.get():
                if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                    pygame.quit()
                    exit()
                if event.type == pygame.KEYDOWN and (event.key == pygame.K_SPACE or event.key == pygame.K_UP):
                    if self.playery > -2 * IMAGES['player'][0].get_height():
                        self.player_vel_y = self.player_flap_acc
                        self.player_flapped = True
                        if ENABLE_SOUND and len(SOUNDS) > 0:
                            SOUNDS['wing'].play()

        # checks if player crashed
        crashTest = checkCrash({'x': self.playerx, 'y': self.playery, 'index': self.playerIndex},
                               self.upperPipes, self.lowerPipes)

        # show score in window
        pygame.display.set_caption("Distance: (" + str(self.travelled_distance) + ")")

        if not crashTest["crashed"]:
            self.travelled_distance += 1
        else:
            self.EPOCH += 1
            pygame.display.update()
            if ENABLE_SOUND and len(SOUNDS) > 0:
                SOUNDS['hit'].play()
            return {
                'y': self.playery,
                'crashed': True,
                'crashedAt': crashTest["where"],
                'basex': self.basex,
                'upperPipes': self.upperPipes,
                'lowerPipes': self.lowerPipes,
                'score': self.score,
                'player_vel_y': self.player_vel_y,
                'player_rot': self.player_rot
            }

        # check for score
        playerMidPos = self.playerx + IMAGES['player'][0].get_width() / 2
        for pipe in self.upperPipes:
            pipeMidPos = pipe['x'] + IMAGES['pipe'][0].get_width() / 2
            if pipeMidPos <= playerMidPos < pipeMidPos + 4:
                self.score += 1
                if ENABLE_SOUND and len(SOUNDS) > 0:
                    SOUNDS['point'].play()

        # playerIndex basex change
        if (self.loopIter + 1) % 3 == 0:
            self.playerIndex = next(self.playerIndexGen)
        self.loopIter = (self.loopIter + 1) % 30
        basex = -((-self.basex + 100) % self.baseShift)

        # rotate the player
        if self.player_rot > -90:
            self.player_rot -= self.player_vel_rot

        # player's movement
        if self.player_vel_y < self.player_max_vel_y and not self.player_flapped:
            self.player_vel_y += self.player_acc_y
        if self.player_flapped:
            self.player_flapped = False
            # more rotation to cover the threshold (calculated in visible rotation)
            self.player_rot = 45

        playerHeight = IMAGES['player'][self.playerIndex].get_height()
        self.playery += min(self.player_vel_y, BASEY - self.playery - playerHeight)

        # move pipes to left
        for uPipe, lPipe in zip(self.upperPipes, self.lowerPipes):
            uPipe['x'] += self.player_vel_x
            lPipe['x'] += self.player_vel_x

        # add new pipe when first pipe is about to touch left of screen
        if 0 < self.upperPipes[0]['x'] < 5:
            newPipe = getRandomPipe()
            self.upperPipes.append(newPipe[0])
            self.lowerPipes.append(newPipe[1])

        # remove first pipe if its out of the screen
        if self.upperPipes[0]['x'] < -IMAGES['pipe'][0].get_width():
            self.upperPipes.pop(0)
            self.lowerPipes.pop(0)

        if not AI_ENABLED:
            self.render_game()

        return crashTest

    def render_game(self):
        from lib.assets import IMAGES

        # draw sprites
        SCREEN.blit(IMAGES['background'], (0, 0))

        for uPipe, lPipe in zip(self.upperPipes, self.lowerPipes):
            SCREEN.blit(IMAGES['pipe'][0], (uPipe['x'], uPipe['y']))
            SCREEN.blit(IMAGES['pipe'][1], (lPipe['x'], lPipe['y']))

        SCREEN.blit(IMAGES['base'], (self.basex, BASEY))
        # print score so player overlaps the score
        showScore(self.score)

        # Player rotation has a threshold
        visibleRot = self.player_rot_thr
        if self.player_rot <= self.player_rot_thr:
            visibleRot = self.player_rot

        playerSurface = pygame.transform.rotate(IMAGES['player'][self.playerIndex], visibleRot)
        SCREEN.blit(playerSurface, (self.playerx, self.playery))


