﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    /**
     * Detects colisions with the player on the left side of the screen.
     * The Collider should be extended to 100% of the y-axis.
     */
    void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.gameObject.name.ToLower().Contains("Satellite".ToLower()))
        {
            GameObject.Find("GameController").GetComponent<GameController>().damage += 1;
        }

        Destroy(other.gameObject);
    }
}
