﻿// Leonardo Banh - 3050245
using UnityEngine;
public class SatelliteController : MonoBehaviour
{
    GazeAware2D gazeAware;
    private GameController gc;

    // Start is called before the first frame update
    void Start()
    {
        // We load a random sprite here from "Assets/Resources/Sprites". 
        // The "Resources" folder is mandatory for `Resources.LoadAll`.
        gazeAware = GetComponent<GazeAware2D>();
        gc = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(0, 0, 0.25f, Space.Self);
        this.transform.Translate(-0.025f, 0, 0, Space.World);

        if (gazeAware.HasGazeFocus)
        {          
            gc.FirePhaserBeam(transform.position);
        }
    }
}
