﻿// Leonardo Banh - 3050245
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;

public class GazeAware2D : MonoBehaviour
{
    private Collider2D Col;
    private Camera Cam;
    public bool HasGazeFocus { get; private set; }
    private bool CheckFocus = true;
    private bool Collides;

    private float lastCheckTime = 0;
    private Vector3 lastCheckPos;
    private double secondsForTrigger = 3.0;
    private double limit = 1.0;

    void Start()
    {
        Col = GetComponent<Collider2D>();
        Cam = Camera.main;
    }

    void Update()
    {
        /* if (CheckFocus)
        {
            Vector3 _gazePoint = new Vector3(TobiiAPI.GetGazePoint().Screen.x, TobiiAPI.GetGazePoint().Screen.y, 0);
            Collides = Col.OverlapPoint(Cam.ScreenToWorldPoint(_gazePoint));
            CheckFocus = false;
            Invoke("CheckFocusDelayed", 0.5f);
        } */

        if (CheckFocus)
        {
            Vector3 _gazePoint = new Vector3(TobiiAPI.GetGazePoint().Screen.x, TobiiAPI.GetGazePoint().Screen.y, 0);
            if ((_gazePoint - lastCheckPos).magnitude < limit)
            {
                Collides = Col.OverlapPoint(Cam.ScreenToWorldPoint(_gazePoint));
                Invoke("CheckFocusDelayed", 0.5f);
            }

            CheckFocus = false;
            lastCheckPos = _gazePoint;
        }

        /* if ((Time.time - lastCheckTime) > secondsForTrigger)
        {
            Vector3 _gazePoint = new Vector3(TobiiAPI.GetGazePoint().Screen.x, TobiiAPI.GetGazePoint().Screen.y, 0);
            if ((_gazePoint - lastCheckPos).magnitude < limit)
            {
                Collides = Col.OverlapPoint(Cam.ScreenToWorldPoint(_gazePoint));
                CheckFocus = false;
                Invoke("CheckFocusDelayed", 0.5f);
            }

            lastCheckPos = _gazePoint;
            lastCheckTime = Time.time;
        } */

        if (Collides)
        {
            HasGazeFocus = true;
            Debug.Log("collision");
        }
        else
        {
            HasGazeFocus = false;
        }
    }

    private void CheckFocusDelayed()
    {
        CheckFocus = true;
    }
}