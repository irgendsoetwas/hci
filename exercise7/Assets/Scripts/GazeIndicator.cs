﻿// Leonardo Banh - 3050245
using UnityEngine;
using Tobii.Gaming;

public class GazeIndicator : MonoBehaviour
{
    private Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        GazePoint gazePoint = TobiiAPI.GetGazePoint();
        var tempVec = new Vector3(gazePoint.Screen.x, gazePoint.Screen.y, 0);

        Vector3 tempPoint = cam.ScreenToWorldPoint(tempVec);
        tempPoint.z = 0;
        this.transform.position = tempPoint;
    }
}
