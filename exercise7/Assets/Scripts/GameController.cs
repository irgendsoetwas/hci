﻿// Leonardo Banh - 3050245
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    // Public objects shall be assigned in the Unity Editor via drag and drop
    public GameObject laserbeam;
    public GameObject lasercannon;
    public GameObject spacerock;
    public GameObject satellite;
    public GameObject spaceship;
    public int SpawnIntervalSeconds;

    // Private variables are assigned in the code
    private Camera cam;                           // holds the main camera
    private LineRenderer phaserbeamLinerenderer;  // holds a linerender i.e. the phaserbeam
    private float lastSpawn;                      // remembers timestamp of the last spawned rock
    private float lastPhaserbeam;                 // remembers when the phaser was shot last
    
    // Points:
    public int points;
    public int damage;
    
    // Start is called before the first frame update
    // We use it to assign the private variables
    // Public variables are assigned in the unity editor via drag and drop of the objects onto the script components
    void Start()
    {
        // Assign the Camera
        cam = Camera.main;
        lastPhaserbeam = Time.time;
        
        // Assign the LineRenderer which depicts the phaser beam and hide it in the very beginning
        // Also set the position of the origin of the laesrbeam to the lasercannon which is a GameObj. on the ship
        // And, very important: set the Linerenderer to use the world space coordinate system.
        phaserbeamLinerenderer = laserbeam.GetComponent<LineRenderer>();
        phaserbeamLinerenderer.enabled = false;
        phaserbeamLinerenderer.SetPosition(0, lasercannon.transform.position);
        phaserbeamLinerenderer.useWorldSpace = true;
        
        // Spawns the very first rock. More rocks will be spawned in Update().
        SpawnRock();
        SpawnSatellite();
    }

    private void OnGUI() { GUI.Label(new Rect(10, 10, 2500, 20), "Points: " + points + "    Spaceship repair status: " + (100-(damage*25)) + "%"); }

    // Update is called once per frame
    private void Update()
    {
        // Spawns rocks
        if(Time.time - lastSpawn > SpawnIntervalSeconds) {
            lastSpawn = Time.time;

            if (Random.Range(0, 2) == 1)
            {
                SpawnRock();
            }
            else
            {
                SpawnSatellite();
            }
        }
        
        // Ends the game
        if (damage == 4)
        {
            spaceship.GetComponent<Rigidbody2D>().gravityScale = 1.0f;
            Destroy(laserbeam);
        }

    }

    private void SpawnRock()
    {
        var rndFloat = Random.Range(-5f, 5f);
        var spawnedRock = Instantiate(spacerock);
        spawnedRock.transform.position = new Vector3(13, rndFloat, 0);
        Debug.Log("++ Spawning Rock.");
    }
    private void SpawnSatellite()
    {
        float rndFloat = Random.Range(-5f, 5f);
        GameObject sat = Instantiate(satellite);
        var randomInt = Random.Range(0, 2); // second parameter is exclusive for integers
        var sprites = Resources.LoadAll<Sprite>("Sprites");
        sat.GetComponent<SpriteRenderer>().sprite = sprites[randomInt];
        sat.transform.position = new Vector3(13, rndFloat, 0);
        Debug.Log("++ Spawning Satellite.");
    }

    public void FirePhaserBeam(Vector3 target)
    {
        Vector3 origin = lasercannon.transform.position;

        phaserbeamLinerenderer.enabled = true;
        
        // Perform a Raycast to determine which object (Rock/Satellite) was hit
        var heading = target - origin;
        var distance = heading.magnitude;
        var hit = Physics2D.Raycast(origin, heading, distance);
        
        if (hit.collider != null) // collider hit something
        {
            // If something except the Player was hit we need to adjust the LineRenderer
            if (!hit.transform.gameObject.tag.Contains("Player"))
            {
                Debug.Log("Phaser hit: " + hit.transform.gameObject + " after distance: " + hit.distance);
                phaserbeamLinerenderer.SetPosition(1, hit.centroid);
                
                // calculate points
                if (hit.transform.name.ToLower().Contains("satellite"))
                {
                    points -= 100;
                } else if (hit.transform.name.ToLower().Contains("spacerock")) {
                    points += 20;
                }
                
                // delete object hit by phaser beam   
                Destroy(hit.transform.gameObject);
            }
            else
            {
                phaserbeamLinerenderer.enabled = false;
            }
        }
        else
        {
            // Point phaser beam to location the user clicked.
            phaserbeamLinerenderer.SetPosition(1, target);
        }
        
        StartCoroutine(HidePhaserBeamAfterTime(1));
    }

    IEnumerator HidePhaserBeamAfterTime(int time)
    {
        yield return new WaitForSeconds(time);
        phaserbeamLinerenderer.enabled = false;
    }
}
