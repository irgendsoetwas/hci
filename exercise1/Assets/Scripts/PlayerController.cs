﻿// Leonardo Banh - matriculation no: 3050245
// color scheme:
// 330,502,455
// rgb(74, 246, 199)

using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerController : MonoBehaviour
{
    public Camera cam;
    public LineRenderer lineRenderer;
    public GameOver gameOver;

    private int _points;
    private int _damage;
    private bool _canShoot = true;

    private readonly string[] _pflist = {"astronaut", "gps", "satellite", "Meteor1", "Meteor2", "Meteor3"};

    private void Start()
    {
        Invoke(nameof(SpawnObject), 1);
    }

    void Update()
    {
        //Debug.Log("Can shoot? " + _canShoot);

        if (_canShoot && Input.GetMouseButtonDown(0))
        {
            _canShoot = false;

            Vector2 mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            var tempPoint =
                cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, cam.transform.position.z * -1));
            Vector3 point = new Vector3(tempPoint.x, tempPoint.y, tempPoint.z);

            Ray laserPointer = new Ray(transform.position + Vector3.right, point);

            if (lineRenderer != null)
            {
                lineRenderer.enabled = true;
                lineRenderer.SetPosition(0, laserPointer.origin);
                lineRenderer.SetPosition(1, point);
            }

            RaycastHit2D hit = Physics2D.Linecast(laserPointer.origin, point);

            if (hit.collider != null)
            {
                GameObject go = hit.collider.gameObject;
                string hitObj = go.name;
                string goTag = go.tag;
                Vector3 hitPoint = hit.point;

                Debug.Log($"Hit: {hitObj} at {hitPoint} with {goTag}");

                if (goTag.Equals("SpaceDebris"))
                {
                    _points += 20;
                    Destroy(go);
                }
                else if (goTag.Equals("Humanoid"))
                {
                    _points -= 100;
                    Destroy(go);
                }
            }

            StartCoroutine(HideRay());
            StartCoroutine(EnableShooting());
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log($"We got hit by {other.gameObject.name}!");
        _damage++;

        Destroy(other.gameObject);

        if (_damage > 3)
        {
            Debug.Log("Game Over!");
            gameOver.EndGame(_points);
        }
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 2500, 20), "Points: " + _points + " Health: " + (100 - (_damage * 25)) + "%");
    }

    private IEnumerator HideRay()
    {
        yield return new WaitForSeconds(0.7f);
        lineRenderer.enabled = false;
    }

    private IEnumerator EnableShooting()
    {
        yield return new WaitForSeconds(2);
        _canShoot = true;
    }

    private void SpawnObject()
    {
        CancelInvoke();

        int randomNum = Random.Range(0, _pflist.Length - 1);
        var objTemp = Resources.Load(_pflist[randomNum]);

        Vector3 spawnPosition = new Vector3(13, Random.Range(-4, 4), 0);
        Instantiate(objTemp, spawnPosition, Quaternion.identity);

        Invoke(nameof(SpawnObject), Random.Range(2, 5));
    }
}