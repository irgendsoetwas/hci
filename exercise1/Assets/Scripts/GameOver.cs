﻿// Leonardo Banh - matriculation no: 3050245

using UnityEngine;

public class GameOver : MonoBehaviour
{
    public GameObject player;

    private bool _end;
    private int _points;

    private readonly GUIStyle _style = new GUIStyle();

    public void EndGame(int points)
    {
        Debug.Log("GameOver screen called");

        _end = true;

        Debug.Log("Destroying Player ship");
        Destroy(player);

        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Humanoid");
        Debug.Log($"Destroying {gameObjects.Length} humanoid objects in space");
        for (var i = 0; i < gameObjects.Length; i++)
        {
            Destroy(gameObjects[i]);
        }

        GameObject[] gameObjects2 = GameObject.FindGameObjectsWithTag("SpaceDebris");
        Debug.Log($"Destroying {gameObjects2.Length} space debris objects in space");
        for (var i = 0; i < gameObjects2.Length; i++)
        {
            Destroy(gameObjects2[i]);
        }

        _points = points;
    }

    private void OnGUI()
    {
        if (_end)
        {
            _style.fontSize = 40;
            _style.normal.textColor = Color.white;
            GUI.Label(new Rect(Screen.width / 2 - 250, Screen.height / 2 - 25, 2500, 20),
                "Game Over! Your score: " + _points, _style);
        }
    }
}