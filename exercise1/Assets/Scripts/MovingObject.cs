﻿// Leonardo Banh - matriculation no: 3050245

using UnityEngine;
using Random = UnityEngine.Random;

public class MovingObject : MonoBehaviour
{
    private int _moveSpeed;
    private int _rotationFactor;

    private void Start()
    {
        _moveSpeed = Random.Range(1, 3);
        _rotationFactor = Random.Range(20, 50);
    }

    void Update()
    {
        transform.Translate(_moveSpeed * Time.deltaTime * Vector3.left, Space.World);
        //transform.Rotate(Time.deltaTime * 50 * Vector3.up, Space.Self);
        transform.Rotate(0, 0, Time.deltaTime * _rotationFactor, Space.Self);


        if (transform.position.x < -15)
        {
            Destroy(gameObject);
        }
    }
}