﻿// Author: Leonardo Banh
// Matriculation No 3050245

using UnityEngine;

public class TimerManager : MonoBehaviour
{
    private float _startTime;
    private float _endTime;
    private float _elapsedTime;

    public GameObject endText;
    public SoundFXRef endSound;

    public void StartTimer()
    {
        _startTime = Time.time;
    }

    public void EndTimer()
    {
        _endTime = Time.time;
        _elapsedTime = _endTime - _startTime;
        Debug.Log("Elapsed Time: " + _elapsedTime);

        endText.SetActive(true);
        endText.GetComponent<TextMesh>().text += "\nTime: " + _elapsedTime + "s";
        endSound.PlaySound();
    }
}