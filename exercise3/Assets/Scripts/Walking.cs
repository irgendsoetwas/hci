﻿// Author: Leonardo Banh
// Matriculation No 3050245

using System;
using UnityEngine;

public class Walking : MonoBehaviour
{
    // 5 mod 5 = 0
    private readonly float Acceleration = 5;
    public GameObject forwardDirection;

    public bool isActive;

    void Update()
    {
        if (isActive)
        {
            if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger) || Input.GetKeyDown(KeyCode.B))
            {
                transform.position += Acceleration * Time.deltaTime * forwardDirection.transform.forward;
            }
        }
    }
}