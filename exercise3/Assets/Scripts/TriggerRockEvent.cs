﻿// Author: Leonardo Banh
// Matriculation No 3050245

using UnityEngine;

public class TriggerRockEvent : MonoBehaviour
{
    public GameObject rock;
    private Rigidbody _rigidbody;

    void Start()
    {
        if (rock != null)
        {
            _rigidbody = rock.GetComponent<Rigidbody>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Triggered zone to let the rock fall down");
        _rigidbody.useGravity = true;
    }
}