﻿// Author: Leonardo Banh
// Matriculation No 3050245

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTrigger : MonoBehaviour
{
    public GameObject timerManager;

    private void OnTriggerEnter(Collider other)
    {
        timerManager.GetComponent<TimerManager>().StartTimer();
    }
}