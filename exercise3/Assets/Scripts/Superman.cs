﻿// Author: Leonardo Banh
// Matriculation No 3050245

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Superman : MonoBehaviour
{
    private OVRPlayerController _ovrPlayerController;
    private bool _isFlying;
    private readonly float Acceleration = 5;
    
    public GameObject forwardDirection;
    public bool isActive;

    // 4 mod 5 = 4
    void Start()
    {
        _ovrPlayerController = GetComponent<OVRPlayerController>();
        if (forwardDirection == null)
        {
            forwardDirection = GameObject.Find("CenterEyeAnchor");
        }
    }

    void Update()
    {
        if (isActive)
        {
            if (OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad) || Input.GetKeyDown(KeyCode.B))
            {
                _isFlying = !_isFlying;
                                 
                if (_isFlying)
                {
                    _ovrPlayerController.GravityModifier = 0f;
                    Vector3 tempPos = transform.position;
                    transform.position = new Vector3(tempPos.x, tempPos.y + 3, tempPos.z);
                }
                else
                {
                    _ovrPlayerController.GravityModifier = 0.5f;
                }
            }
                                 
            Vector3 rotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTouch).eulerAngles;
                                 
            if (_isFlying && rotation.z < 100)
            {
                transform.position += Acceleration * Time.deltaTime * forwardDirection.transform.forward;
            }
                                 
            if (_isFlying && rotation.z > 260 && rotation.z < 300)
            {
                transform.position -= Acceleration * Time.deltaTime * forwardDirection.transform.forward;
            }
        }
    }
}