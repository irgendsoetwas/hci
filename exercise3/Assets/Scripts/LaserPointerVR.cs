﻿// Author: Leonardo Banh
// Matriculation No 3050245

using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;

public class LaserPointerVR : MonoBehaviour
{
    #region Variables

    [Space(10)] public Transform leftHandAnchor;
    public Transform rightHandAnchor;
    public LineRenderer lineRenderer;
    public float maxRayDistance = 50.0f;

    public LayerMask excludeLayers;

    public GameObject PlayerController;

    private bool _started;

    private bool _showRay = true;

    Transform Pointer
    {
        get
        {
            OVRInput.Controller controller = OVRInput.GetConnectedControllers();
            if ((controller & OVRInput.Controller.LTrackedRemote) != OVRInput.Controller.None)
            {
                return leftHandAnchor;
            }

            if ((controller & OVRInput.Controller.RTrackedRemote) != OVRInput.Controller.None)
            {
                return rightHandAnchor;
            }

            return null;
        }
    }

    #endregion

    void Start()
    {
        if (leftHandAnchor == null)
        {
            Debug.LogWarning("Assign LeftHandAnchor in the inspector!");
            GameObject left = GameObject.Find("LeftHandAnchor");
            if (left != null)
            {
                leftHandAnchor = left.transform;
            }
        }

        if (rightHandAnchor == null)
        {
            Debug.LogWarning("Assign RightHandAnchor in the inspector!");
            GameObject right = GameObject.Find("RightHandAnchor");
            if (right != null)
            {
                rightHandAnchor = right.transform;
            }
        }

        if (lineRenderer == null)
        {
            Debug.LogWarning("Assign a line renderer in the inspector!");
            lineRenderer = gameObject.AddComponent<LineRenderer>();
            lineRenderer.shadowCastingMode = ShadowCastingMode.Off;
            lineRenderer.receiveShadows = false;
            lineRenderer.widthMultiplier = 0.02f;
            lineRenderer.material.color = Color.red;
            lineRenderer.startColor = Color.red;
            lineRenderer.endColor = Color.red;
        }
    }

    void Update()
    {
        Transform pointer = Pointer;
        if (pointer == null)
        {
            //return;
        }

        lineRenderer.enabled = _showRay;

        Ray laserPointer = new Ray(pointer.position, pointer.forward);

        if (lineRenderer != null)
        {
            lineRenderer.SetPosition(0, laserPointer.origin);
            lineRenderer.SetPosition(1, laserPointer.origin + laserPointer.direction * maxRayDistance);
        }

        RaycastHit hit;
        if (Physics.Raycast(laserPointer, out hit, maxRayDistance))
        {
            if (lineRenderer != null)
            {
                lineRenderer.SetPosition(1, hit.point);
            }
        }

        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit2;
            
            if (Physics.Raycast(laserPointer, out hit2, maxRayDistance, ~excludeLayers))
            {
                string elementName = hit2.collider.gameObject.name;
                switch (elementName)
                {
                    case "Walking":
                        PlayerController.GetComponent<Walking>().isActive = true;
                        PlayerController.GetComponent<Superman>().isActive = false;
                        break;
                    case "Superman":
                        PlayerController.GetComponent<Walking>().isActive = false;
                        PlayerController.GetComponent<Superman>().isActive = true;
                        break;
                }
            }
        }

        else if (OVRInput.GetDown(OVRInput.Button.One) || Input.GetKeyDown(KeyCode.Q))
        {
            _showRay = !_showRay;
        }
    }
}